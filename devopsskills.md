# Acquiring DevOps Skills 
by @pavelsuchman



### About me:
<img src="images/bunch_of_kids.jpg">
Notes:  
- I am a freelance DevOps cosultant
- 4+ years of DevOps experience, 14+ years in IT/OPS



## Cockroft's definition of DevOps:
Ops self-service by developers

Note:
There is also an official [description](http://en.wikipedia.org/wiki/Devops) that you can read on Wikipedia...



## Why is learning DevOps-fu important?


### It will make you a better developer
Note: The best developers I know understand well the system/operations side of things.  
It could be a coincidence, but probably not.
Your market value will grow  
You could be able to save the day


### There could be nobody else up to the job
Note: Expect 6+ month to fill a DevOps position  
Good DevOps consultatnts are also hard to find and they charge a lot (Winks;)



## There are 2 main roads to DevOps:
- Dev to DevOps
- Ops to DevOps
Note: Anecdotically there are more from the 1rst kind, I am coming from the 2nd.  
What about QA?


## What are the challenges on each way?
Note: Each side has to learn some of the skills of the other and then some...  
Everybody is weak at communications  
Basically Devs have to learn to do operations and Ops to do Development  


## Ops to Devops
Note: I  learned Python, source control and the idea of working with 3rd party APIs
AWS was the eye opener for me  


##Dev to DevOps: the objections
- It's dirty work
- It's too hard (or too easy;)
- Let the Ops guys do it (Not my responsibility)



## It's not beneath you!
### Plus, it's fun
Note: From my limited and selfserving POV, the best developers are interested in operations    
Probably just because they are interested in full stack (AKA There is life beyonf your IDE...)



#Skills, tips and techniques



#Use Linux (as a desktop)
Note: Most of SaaS companies run on Linux, some on Windows.   
Yet most of developers use Windows as a desktop, some Mac...  
You should use Linux because that's where your application lives.


##Have an open terminal window
- Learn CLI the hard way
Note: http://cli.learncodethehardway.org/book/


## Password-less [ssh](http://www.debian-administration.org/articles/152)
Because automation!!!


# Vim
- Because no Sublime in production!
- [Learn Vim Progressively](http://yannesposito.com/Scratch/en/blog/Learn-Vim-Progressively/)
<img src="images/learning_curves.jpg">



#bash
- The most basic way to automate things in linux
- Don't overdo it!
- An excellent [reference](http://www.tldp.org/LDP/abs/html/)


##AWS is a set of APIs, not a webapp
### And not an old school hosting provider!
Note: At any given time only ~30% of features make it to console, the rest is in API
Some companies used to start a machine from the console, treat it like a regular server and proclaim  
that they are "on Amazon" - Fail!



#Ops topics for SaaS companies:


###Web servers and application servers and how do they talk to each other  
- Tomcat/Uwsgi/Passenger vs Apache/Nginx
- Raw Node.js - DON'T!
Note: App Servers like Tomcat for Java, uwsgi for Python, Passenger for Ruby
Web servers were developed for serving static files - use it!  
Nginx is faster -> event loop, vs threads in Apache


###Database maintenance - backups, sharding and replication
Note: 
Backups vs replication
Sharding makes everything more complicated


##Logging and searching the logs
- [Loggly](https://www.loggly.com/)
- [Papertrail](https://papertrailapp.com/)
- [Logstash + ElasticSearch](http://logstash.net/)


#Monitoring and alerting
- [Serverdensity](https://www.serverdensity.com/)
- [Stackdriver](https://www.stackdriver.com/)
- [Pingdom](https://www.pingdom.com/)
- DON'T use NAGIOS!!!


#Staging on your laptop
- [Vagrant](http://www.vagrantup.com/)
- [Docker](https://www.docker.io/)
- Vmware
- Did I mention AWS?
Note: Not trivial because of SOA.



###Configuration management - just don't! 
- Ok, listen to Evgeny's [presentation](https://www.youtube.com/watch?feature=player_detailpage&v=jNQ2w6qL-KQ) and decide when it's time...



# Tips:
- Don't be afraid to break things (As long as you know how to fix them;) - that's the way to learn
- BE afraid to corrupt data or kill a single critical service
- Make your shit repeatable!



# Principles:
- Always start with a service vs rolling your own - your time is more expensive
- In production have at least 2 of everything
- JFDI!



# Questions?
@pavelsuchman  
pavel.suchman@gmail.com
